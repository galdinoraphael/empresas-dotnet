﻿namespace MeuIMDb.Models
{
    public class Usuario
    {
        public int Id { set; get; }
        public string Nome { set; get; }
        public string Email { set; get; }
        public string Senha { set; get; }
        public bool Ativo { get; private set; }
        public string Role { set; get; }

        void setarAtivo(bool valor)
        {
            Ativo = valor;
        }
    }
}
