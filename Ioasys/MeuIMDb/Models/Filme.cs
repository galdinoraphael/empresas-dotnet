﻿namespace MeuIMDb.Models
{
    public class Filme
    {
        public int Id { set; get; }
        public string Nome { get; set; }
        public int Pontuacao { get; set;}
        public string Diretor { get; set; }
        public string Genero { get; set; }
        public string[] Atores { get; set; }
    }
}
