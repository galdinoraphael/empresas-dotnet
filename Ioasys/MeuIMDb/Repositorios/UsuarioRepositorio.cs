﻿using MeuIMDb.Models;
using System.Collections.Generic;
using System.Linq;

namespace MeuIMDb.Repositorios
{
    public static class UsuarioRepositorio
    {
        public static Usuario Get(string username, string password)
        {
            var usuarios = new List<Usuario>();
            usuarios.Add(new Usuario { Id = 1, Nome = "batman", Senha = "batman", Role = "manager" });
            usuarios.Add(new Usuario { Id = 2, Nome = "robin", Senha = "robin", Role = "employee" });

            return usuarios.Where(x => x.Nome.ToLower() == username.ToLower() && x.Senha == x.Senha).FirstOrDefault();
        }
    }
}
